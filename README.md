# Profiles

Repository to handle the profiles information needed for SiteMon

## Structure
The way metrics are combined to reach the site status is by using intermediate aggregations:
   * Metrics -> Endpoint -> Flavour -> Site

## How to understand the profiles
   * VO: Represents the VO for which this profile is active
   * ONLY_PRODUCTION: Wether to use or not metrics attached to non production sites/endpoints when computing the status
   * FLAVOURS: List of flavours used within the algorithm definition
      * NAME: Name of the flavour
      * METRICS: List of metrics used for the computation of the endpoint-flavour status
      * ENDPOINT_OPERATOR: Specifies how metrics have to be combined to generate the status for the endpoint-flavour (OR = any, AND = all)
   * PROFILE_ALGORITHM: Specifies how different endpoint are combined to compute a flavour status (i.e: [OR, [XROOTD] means any endpoint to be OK) and also how flavours are combined among them to compute the site status (i.e: [AND, [OR, [XROOTD]], [AND, [WEBDAV]]] meand "any XROOTD endpoint" AND "all WEBDAV endpoints")
